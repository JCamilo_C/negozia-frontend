export interface Diary {
    type: string;
    phone: string;
}


export interface Contact {
    _id: string;
    firstName: string;
    lastName: string;
    gender: string;
    diary: Diary[];
    email: string;
}

export interface ListContact {
    _id: string;
    firstName: string;
    lastName: string;
    email: string;
}

export interface GraphCommerces {
    name: string;
    sales: string;
}
