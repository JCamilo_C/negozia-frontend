
export interface SubMenu {
    titulo: string;
    url: string;
}

export interface Option {
    titulo: string;
    icono: string;
    submenu?: SubMenu[];
    url: string;
}

export interface Menu {
    nombreModulo: string;
    opciones: Option[];
}

export const initStateMenu: Menu = {
    nombreModulo: 'Default',
    opciones: []
};
