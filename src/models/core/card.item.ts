import { Contact, ListContact } from '../contact.model';

export interface Item {
    id: string;
    fullname: string;
    email: string;
}

export class ItemMapper {

    ContactToItem = (model: ListContact): Item => {
        return {
            id: model._id,
            fullname: `${model.firstName} ${model.lastName}`,
            email: model.email
        };
    }
}
