import { Injectable } from '@angular/core';
import { Actions, Effect, ofType, createEffect } from '@ngrx/effects';
import { ContactService } from 'src/app/services/api/contact.service';

import { switchMap, catchError } from 'rxjs/operators';

import * as fromActions from '../actions';

import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ContactEffect {
    constructor(private action$: Actions, private contactService: ContactService, private toastr: ToastrService) {}

    LoadingAllContacts$ = createEffect( () => this.action$.pipe(
        ofType(fromActions.LoadingAllContacts),
        switchMap( () => this.contactService.getAllContacts().pipe(
            switchMap( r => [fromActions.AllContacts({payload: r})]),
            catchError( e => [fromActions.ErrorLoadContacts(e)])
        ))
    ));

    LoadingContactDetail$ = createEffect( () => this.action$.pipe(
        ofType(fromActions.LoadingContactDetail),
        switchMap( (action) => this.contactService.getContactDetail(action.id).pipe(
            switchMap( r => [fromActions.ContactDetail({payload: r})]),
            catchError( e => [fromActions.ErrorLoadContacts(e)])
        ))
    ));

    LoadingCreateContact$ = createEffect( () => this.action$.pipe(
        ofType(fromActions.LoadingCreateContact),
        switchMap( (action) => this.contactService.postCreateContact(action.payload).pipe(
            switchMap( r => {
                this.toastr.success('Bien', `Se creo correctamente el usuario ${r.firstName}`);
                return [fromActions.CreateContact({ payload: r})];
            }),
            catchError( e => {
                this.toastr.success('Bien', `ocurrio un error => ${JSON.stringify(e)}`);
                return [fromActions.ErrorLoadContacts(e)];
            })
        ))
    ));

    LoadingDeleteContact$ = createEffect( () => this.action$.pipe(
        ofType(fromActions.LoadingDeleteContact),
        switchMap( (action) => this.contactService.deleteContact(action.id).pipe(
            switchMap( () => {
                this.toastr.success('Bien', 'Se elimino correctamente el registro');
                return [fromActions.DeleteContact({id: action.id })];
            }),
            catchError( e => [fromActions.ErrorLoadContacts(e)])
        ))
    ));

    LoadingUpdateContact$ = createEffect( () => this.action$.pipe(
        ofType(fromActions.LoadingUpdateContact),
        switchMap( (action) => this.contactService.updateContact(action.id, action.payload).pipe(
            switchMap( (r) => {
                this.toastr.success('Bien', 'Se actualizo correctamente el registro');
                return [fromActions.UpdateContact({ id: r._id, payload: action.payload })];
            }),
            catchError( e => [fromActions.ErrorLoadContacts(e)])
        ))
    ));

}
