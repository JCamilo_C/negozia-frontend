import { Action } from '@ngrx/store';
import { Menu } from '../../models/core/sidebar.model';

export enum UiTypeActions {
    SET_MENU = '[UI] Set menu',
    SET_MODULE_NAME = '[UI] Set module name',
    TOGGLE_MENU = '[UI] Change state menu',
}

export class SetMenu implements Action {
    readonly type = UiTypeActions.SET_MENU;
    constructor(public payload: Menu) {}
}

export class SetModuleName implements Action {
    readonly type = UiTypeActions.SET_MODULE_NAME;
    constructor(public payload: string) {}
}

export class ToggleMenu implements Action {
    readonly type = UiTypeActions.TOGGLE_MENU;
}

export type actions = SetMenu | SetModuleName | ToggleMenu;
