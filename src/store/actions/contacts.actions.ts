import { GraphCommerces, Contact, ListContact } from '../../models/contact.model';
import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';

export enum ContactsTypeAction {
    LOADING_ALL_CONTACTS = '[CONTACTS] Loading all contacts',
    ALL_CONTACTS = '[CONTACTS] all contacts',
    LOADING_CONTACT_DETAIL = '[CONTACTS] Loading search contact',
    CONTACT_DETAIL = '[CONTACTS] get contact',
    LOADING_CREATE_CONTACT = '[CONTACTS] Loading create contact',
    CREATE_CONTACT = '[CONTACTS] Create contact',
    LOADING_DELETE_CONTACT = '[CONTACTS] Loading delete contact',
    DELETE_CONTACT = '[CONTACTS] delete contact',
    LOADING_UPDATE_CONTACT = '[CONTACTS] Loading update contact',
    UPDATE_CONTACT = '[CONTACTS] update contact',
    LOADING_GRAPH_CONTACTS = '[CONTACTS] Loading contacts sales',
    GRAPH_CONTACTS = '[CONTACTS] sales contacts',
    CONTACTS_ERROR_LOAD = '[CONTACTS] error loading',
}

export const LoadingAllContacts = createAction(
    ContactsTypeAction.LOADING_ALL_CONTACTS
);

export const AllContacts = createAction(
    ContactsTypeAction.ALL_CONTACTS,
    props<{payload: ListContact[]}>()
);

export const LoadingContactDetail = createAction(
    ContactsTypeAction.LOADING_CONTACT_DETAIL,
    props<{ id: string}>()
);

export const ContactDetail = createAction(
    ContactsTypeAction.CONTACT_DETAIL,
    props<{ payload: Contact}>()
);

export const LoadingCreateContact = createAction(
    ContactsTypeAction.LOADING_CREATE_CONTACT,
    props<{ payload: Contact }>()
);

export const CreateContact = createAction(
    ContactsTypeAction.CREATE_CONTACT,
    props<{ payload: ListContact}>()
);

export const LoadingDeleteContact = createAction(
    ContactsTypeAction.LOADING_DELETE_CONTACT,
    props<{ id: string}>()
);

export const DeleteContact = createAction(
    ContactsTypeAction.DELETE_CONTACT,
    props<{ id: string}>()
);

export const LoadingUpdateContact = createAction(
    ContactsTypeAction.LOADING_UPDATE_CONTACT,
    props<{ id: string, payload: Contact }>()
);

export const UpdateContact = createAction(
    ContactsTypeAction.UPDATE_CONTACT,
    props<{ id: string, payload: Contact}>()
);

export const ErrorLoadContacts = createAction(
    ContactsTypeAction.CONTACTS_ERROR_LOAD,
    props<{payload: HttpErrorResponse}>()
);
