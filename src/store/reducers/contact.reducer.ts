import { Contact, GraphCommerces, ListContact } from '../../models/contact.model';
import { HttpErrorResponse } from '@angular/common/http';
import * as contactsActions from '../actions/contacts.actions';
import { AppState } from '../app.reducer';
import { createReducer, on, Action } from '@ngrx/store';

export interface ContactState {
    loading: boolean;
    contacts: ListContact[];
    loading_detail: boolean;
    contact: Contact;
    sales: GraphCommerces[];
    error: HttpErrorResponse;
}

const initState: ContactState = {
    loading: false,
    contacts: [],
    loading_detail: false,
    contact: null,
    sales: [],
    error: null
};

export interface ContactAppState extends AppState {
    contact: ContactState;
}

const Reducer = createReducer(
    initState,
    on(contactsActions.LoadingAllContacts,
       contactsActions.LoadingCreateContact,
       (state) => ({...state, loading: true})),
    on(contactsActions.LoadingContactDetail,
       contactsActions.LoadingDeleteContact,
       contactsActions.LoadingUpdateContact,
       (state) => ({...state, loading_detail: true})),
    on(contactsActions.AllContacts, (state, {payload}) => ({...state, loading: false, contacts: [...payload]})),
    on(contactsActions.ContactDetail, (state, {payload}) => ({...state, loading_detail: false, contact: {...payload}})),
    on(contactsActions.CreateContact, (state, {payload}) => ({...state, loading: false, contacts: [...state.contacts, {...payload}] })),
    on(contactsActions.UpdateContact, (state, {id, payload }) => ({
        ...state,
        loading_detail: false,
        contact: {...payload},
        contacts: [...state.contacts.map( x => {
            if (x._id === id) {
                console.log(payload);
                x = {...payload};
            }
            return x;
        })]
    })),
    on(contactsActions.DeleteContact, (state, {id}) => ({
        ...state,
        loading_detail: false,
        contact: null,
        contacts: [...state.contacts.filter( x => x._id !== id)]
    }))
);

export function contactReducer(state = initState, action: Action): ContactState {
   return Reducer(state, action);
}
