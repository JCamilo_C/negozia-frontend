import { Menu, initStateMenu } from '../../models/core/sidebar.model';
import * as actionsUi from '../actions/ui.actions';

export interface UiState {
    menu: Menu;
    sidebarActivate: boolean;
}

const initState: UiState = {
    menu: initStateMenu,
    sidebarActivate: false
};

export function UiReducer(state = initState, action: actionsUi.actions): UiState {
    switch (action.type) {
        case actionsUi.UiTypeActions.SET_MENU:
            return {
                ...state,
                menu: {...action.payload}
            };
        case actionsUi.UiTypeActions.SET_MODULE_NAME:
            return {
                ...state,
                menu: {
                    ...state.menu,
                    nombreModulo: action.payload
                }
            };
        case actionsUi.UiTypeActions.TOGGLE_MENU:
            return {
                ...state,
                sidebarActivate: !state.sidebarActivate
            };
        default:
            return {
                ...state
            };
    }
}
