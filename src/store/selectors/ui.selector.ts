import { AppState } from '../app.reducer';
import { createSelector } from '@ngrx/store';
import { UiState } from '../reducers/ui.reducer';

const SelectUiState = (state: AppState) => state.ui;

export const GetMenu = createSelector(
    SelectUiState,
    (ui: UiState) => ui.menu
);


export const GetStateSidebar = createSelector(
    SelectUiState,
    (ui: UiState) => ui.sidebarActivate
);
