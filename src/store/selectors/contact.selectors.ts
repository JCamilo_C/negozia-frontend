import { ContactAppState, ContactState } from '../reducers/contact.reducer';
import { createSelector } from '@ngrx/store';
import { ItemMapper } from '../../models/core/card.item';


const SelectContactState = (state: ContactAppState): ContactState => state.contact;

export const loadingContacts = createSelector(
    SelectContactState,
    (state) => state.loading
);

export const loadingContactDetail = createSelector(
    SelectContactState,
    (state) => state.loading_detail
);

export const getContacts = createSelector(
    SelectContactState,
    (state) => {
        const mapper = new ItemMapper();
        return [...state.contacts.map(mapper.ContactToItem)];
    }
);

export const getDetailContact = createSelector(
    SelectContactState,
    (state) => state.contact
);


export const getSalesCommerces = createSelector(
    SelectContactState,
    (state) => {
        return [...state.sales];
    }
);
