import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxMaskModule } from 'ngx-mask';
import { PipesModule } from '../pipes/pipes.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { CardContactComponent } from './card-contact/card-contact.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { LoadingContactComponent } from './loading-contact/loading-contact.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CardContactComponent,
    ContactDetailComponent,
    LoadingContactComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    PipesModule,
    NgxMaskModule.forRoot(),
    PerfectScrollbarModule,
    NgxSkeletonLoaderModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CardContactComponent,
    ContactDetailComponent
  ]
})
export class ComponentsModule { }
