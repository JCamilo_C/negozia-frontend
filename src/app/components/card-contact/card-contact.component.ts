import { Component, OnInit, Input, Renderer2, ElementRef } from '@angular/core';
import { Item } from '../../../models/core/card.item';
import { Contact } from '../../../models/contact.model';
import { DataService } from '../../services/core/data.service';

@Component({
  selector: 'app-card-contact',
  templateUrl: './card-contact.component.html',
  styleUrls: ['./card-contact.component.scss']
})
export class CardContactComponent implements OnInit {

  @Input() itemContacts: Item[] = [];
  AllItems: Item[] = [];

  constructor(private dataService: DataService<string>, private renderer2: Renderer2) { }

  ngOnInit(): void {
    this.AllItems = [...this.itemContacts];
  }

  Buscar(e: any) {
    let search: string = e.target.value as string;
    if (search !== '') {
      search = search.toLowerCase().toString();
      this.itemContacts = this.itemContacts.filter( item => {
        return item.fullname.toLowerCase().trim().indexOf(search) > -1 || item.email.toLowerCase().trim().indexOf(search) > -1;
      });
    } else {
      this.itemContacts = this.AllItems;
    }
  }

  setContact(contact: string, element) {
    const el = document.querySelectorAll('.customItem');
    [].forEach.call(el, (elementFirts) => {
      if (elementFirts.classList.contains('activate')) {
        elementFirts.classList.remove('activate');
      }
    });
    this.renderer2.addClass(element, 'activate');
    this.dataService.setData(contact);
  }

}
