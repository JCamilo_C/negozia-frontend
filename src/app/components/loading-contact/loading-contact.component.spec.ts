import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingContactComponent } from './loading-contact.component';

describe('LoadingContactComponent', () => {
  let component: LoadingContactComponent;
  let fixture: ComponentFixture<LoadingContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
