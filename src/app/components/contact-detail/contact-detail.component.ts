import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ContactAppState } from '../../../store/reducers/contact.reducer';
import { DataService } from '../../services/core/data.service';
import { Observable, of } from 'rxjs';
import { Contact, Diary } from '../../../models/contact.model';

import * as selectors from '../../../store/selectors';
import * as fromAction from '../../../store/actions';
import { filter, take } from 'rxjs/operators';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent implements OnInit {

  contactDetail$: Observable<Contact> = of(null);
  loadingContact$: Observable<boolean> = of(false);

  updateForm: FormGroup;

  updateActivate = false;

  constructor(private store: Store<ContactAppState>, private dataService: DataService<string>) {
   }

  ngOnInit(): void {
    this.loadingContact$ = this.store.select(selectors.loadingContactDetail);
    this.contactDetail$ = this.store.select(selectors.getDetailContact);
    this.dataService.getData().pipe( filter( x => x !== null) ).subscribe( x => {
      this.store.dispatch(fromAction.LoadingContactDetail({id: x}));
      this.updateActivate = false;
    });

    this.contactDetail$.pipe(filter(x => x !== null)).subscribe( x => {
      this.updateForm = new FormGroup({
        firstName: new FormControl(x.firstName, [Validators.required]),
        lastName: new FormControl(x.lastName, [Validators.required]),
        gender: new FormControl(x.gender, [Validators.required]),
        diary: new FormArray([
        ]),
        email: new FormControl(x.email, [Validators.required, Validators.email])
      });
      this.startUpdate(x.diary);
    });
  }

  eliminar() {
    this.contactDetail$.pipe(take(1)).subscribe( x => {
      this.store.dispatch(fromAction.LoadingDeleteContact({id: x._id}));
    });
  }

  onUpdate() {
    this.dataService.getData().pipe( filter( x => x !== null) ).subscribe( x => {
      if (this.updateForm.valid && this.updateActivate) {
        const model: Contact = this.updateForm.value;
        this.store.dispatch(fromAction.LoadingUpdateContact({id: x, payload: model}));
      }
    });
  }

  get ControlTelefonos(): FormArray {
    return this.updateForm.get('diary') as FormArray;
  }

  startUpdate(diario: Diary[]) {
    for (const iterator of diario) {
      this.ControlTelefonos.push(
        new FormGroup({
          type : new FormControl(iterator.type, Validators.required),
          phone : new FormControl(iterator.phone, Validators.required)
        })
      );
    }
  }

  AgregarDir() {
    this.ControlTelefonos.push(
      new FormGroup({
        type : new FormControl('', Validators.required),
        phone : new FormControl('', Validators.required)
      })
    );
  }

  EliminarDir(i: number) {
    this.ControlTelefonos.removeAt(i);
  }

  EliminarUltimo() {
    this.ControlTelefonos.removeAt(this.ControlTelefonos.length - 1);
  }


}
