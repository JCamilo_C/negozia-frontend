import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// MODULOS
import { ComponentsModule } from '../../components/components.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SharedModule } from '../../shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

// ROUTES
import { HomeRoutingModule } from './home.routes';

// STORE
import { contactReducer } from 'src/store/reducers/contact.reducer';
import { ContactEffect } from 'src/store/effects/contact.effect';

// COMPONENTS
import { HomeComponent } from './home.component';
import { ContactsComponent } from './contacts/contacts.component';
import { NewContactComponent } from './new-contact/new-contact.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ContactsComponent,
    HomeComponent,
    NewContactComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    NgxSkeletonLoaderModule,
    PerfectScrollbarModule,
    RouterModule,
    StoreModule.forFeature('contact', contactReducer),
    EffectsModule.forFeature([ContactEffect]),
  ]
})
export class HomeModule { }
