import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store';
import { ContactAppState } from 'src/store/reducers/contact.reducer';
import { Contact } from '../../../../models/contact.model';

import * as fromActions from '../../../../store/actions';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.scss']
})
export class NewContactComponent implements OnInit {

  modelForm: FormGroup;

  constructor(private toastr: ToastrService, private store: Store<ContactAppState>) {
    this.modelForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      diary: new FormArray([
        new FormGroup({
          type: new FormControl('', [Validators.required]),
          phone: new FormControl('', [Validators.required])
        })
      ]),
      email: new FormControl('', [Validators.required, Validators.email])
    });
   }

  ngOnInit(): void { }

  onSubmit() {
    if (this.modelForm.valid) {
      const payload: Contact = this.modelForm.value;
      payload.firstName = payload.firstName.trim();
      payload.lastName = payload.lastName.trim();
      payload.gender = payload.gender.trim();
      payload.email = payload.email.trim();

      this.store.dispatch(fromActions.LoadingCreateContact({payload}));
    } else {
      this.toastr.error('Error', `Debe ingresar todos los campos`);
    }
  }

  get ControlTelefonos(): FormArray {
    return this.modelForm.get('diary') as FormArray;
  }

  AgregarDir() {
    this.ControlTelefonos.push(
      new FormGroup({
        type : new FormControl('', Validators.required),
        phone : new FormControl('', Validators.required)
      })
    );
  }

  EliminarDir(i: number) {
    this.ControlTelefonos.removeAt(i);
  }

  EliminarUltimo() {
    this.ControlTelefonos.removeAt(this.ControlTelefonos.length - 1);
  }

}
