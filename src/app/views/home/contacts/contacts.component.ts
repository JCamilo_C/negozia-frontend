import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';

// MODELOS
import { Item } from '../../../../models/core/card.item';

// STORE
import { ContactAppState } from '../../../../store/reducers/contact.reducer';
import * as selectors from '../../../../store/selectors';
import * as actions from '../../../../store/actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  loading$: Observable<boolean> = of(true);
  contacts$: Observable<Item[]> = of([]);
  selectedCommerce: BehaviorSubject<number> = new BehaviorSubject<number>(null);

  constructor(private store: Store<ContactAppState>) { }

  ngOnInit() {
    this.store.dispatch(new actions.SetModuleName('Contactos')); // Actualizo en nombre de componente de la interfaz
    this.loading$ = this.store.select(selectors.loadingContacts); // Consulto si esta cargando el store
    this.contacts$ = this.store.select(selectors.getContacts); // Obtengo todos los comercios
  }

}
