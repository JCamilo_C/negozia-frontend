import { Component, OnInit } from '@angular/core';

// MODELS
import { Menu } from '../../../models/core/sidebar.model';

// STORE
import { AppState } from '../../../store/app.reducer';
import * as actions from '../../../store/actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  menu: Menu = { // CREACION DEL MENU
    nombreModulo: 'Contactos',
    opciones: [
        {
            titulo: 'Ver contactos',
            icono: 'fas fa-user-plus',
            url: '/Home/Contactos'
        },
        {
          titulo: 'Crear contacto',
          icono: 'far fa-list-alt',
          url: '/Home/ContactoNuevo'
        }
    ]
  };

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.dispatch(new actions.SetMenu(this.menu)); // ESTABLESCO EL MENU EN EL STORE
    this.store.dispatch(actions.LoadingAllContacts()); // CARGO TODOS LOS COMERCIOS STORE
  }

}
