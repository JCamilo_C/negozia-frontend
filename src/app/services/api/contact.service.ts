import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Contact, GraphCommerces, ListContact } from '../../../models/contact.model';
import { ResponseModel } from '../../../models/core/response.model';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private url = `${environment.baseUrl}/contact`;

  constructor(private http: HttpClient) { }

  /**
   * Servicio que obtiene todos los contactos
   * @returns Observable<ListContact[]>
   */
  getAllContacts(): Observable<ListContact[]> {
    return this.http.get<ListContact[]>(this.url);
  }

  /**
   * Servicio que obtiene un contacto en especifico
   * @returns Observable<Contact>
   */
  getContactDetail(id: string): Observable<Contact> {
    return this.http.get<Contact>(`${this.url}/${id}`);
  }

  /**
   * Servicio que crear un contacto en
   * @returns Observable<Contact[]>
   */
  postCreateContact(model: Contact): Observable<ListContact> {
    return this.http.post<ListContact>(`${this.url}`, model);
  }

  /**
   * Servicio que elimina un contacto en especifico
   * @returns Observable<ResponseModel<Contact>>
   */
  updateContact(id: string, contact: Contact): Observable<Contact> {
    return this.http.put<Contact>(`${this.url}/${id}`, contact);
  }

  /**
   * Servicio que elimina un contacto en especifico
   * @returns Observable<ResponseModel<string>>
   */
  deleteContact(id: string): Observable<ResponseModel<string>> {
    return this.http.delete<ResponseModel<string>>(`${this.url}/${id}`);
  }

}
