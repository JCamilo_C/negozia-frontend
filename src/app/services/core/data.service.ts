import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService<T> {

  private data: BehaviorSubject<T> = new BehaviorSubject<T>(null);
  public Data$ = this.data.asObservable();

  constructor() { }

  /**
   * funcion que setea información en mi behavior subject
   * @param d tipo de informacion de conjuto dinamico dependiendo de la clase que se necesite
   */
  setData(d: T) {
    this.data.next(d);
  }

  // Get Single Course by id. will 404 if id not found
  getData(): Observable<T> {
      return this.Data$;
  }
}
