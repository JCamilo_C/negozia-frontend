import { Component, OnInit } from '@angular/core';
import { AppState } from '../../../store/app.reducer';
import { Store } from '@ngrx/store';

import * as actions from '../../../store/actions';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
  }

  toggleSideBar() {
    this.store.dispatch(new actions.ToggleMenu());
  }

}
