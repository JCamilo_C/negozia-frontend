import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Menu } from '../../../models/core/sidebar.model';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.reducer';

import * as selectors from '../../../store/selectors';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  Menu$: Observable<Menu>;
  SidebarState$: Observable<boolean> = of(false);

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.Menu$ = this.store.select(selectors.GetMenu);
    this.SidebarState$ = this.store.select(selectors.GetStateSidebar);
  }

}
