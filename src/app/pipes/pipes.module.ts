import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberParsePipe } from './number-parse.pipe';

@NgModule({
  declarations: [NumberParsePipe],
  imports: [
    CommonModule
  ],
  exports: [NumberParsePipe]
})
export class PipesModule { }
