import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberParse'
})
export class NumberParsePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return Number(value);
  }

}
